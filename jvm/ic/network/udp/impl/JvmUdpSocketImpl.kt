package ic.network.udp.impl


import java.io.IOException
import java.io.InterruptedIOException
import java.net.DatagramSocket
import java.net.DatagramPacket
import java.net.InetAddress

import ic.base.assert.assert
import ic.base.arrays.bytes.ByteArray
import ic.base.arrays.bytes.ext.copy.copyLimit
import ic.base.arrays.ext.length
import ic.base.escape.skippable.skip
import ic.base.escape.skippable.skippable
import ic.base.loop.loop
import ic.base.objects.ext.orSkip
import ic.base.primitives.int32.Int32
import ic.base.throwables.IoException
import ic.ifaces.stoppable.ext.stopNonBlocking
import ic.network.socket.SocketAddress
import ic.parallel.funs.*
import ic.parallel.funs.doinparallel.doInParallel
import ic.parallel.thread.Thread
import ic.parallel.thread.funs.stopThread
import ic.util.log.logW
import ic.util.time.duration.ext.sec

import ic.network.udp.UdpSocket


class JvmUdpSocketImpl : UdpSocketImpl {


	private var socket : UdpSocket? = null

	private var jvmSocket : DatagramSocket? = null

	private var thread : Thread? = null


	@Throws(IoException::class)
	override fun open (socket: UdpSocket, port: Int32) {

		assert { this.socket == null }
		this.socket = socket

		assert { this.jvmSocket == null }
		val jvmSocket = (
			try {
				DatagramSocket(port)
			} catch (_: IOException) {
				throw IoException
			}
		).also { this.jvmSocket = it }

		assert { thread == null }
		thread = doInBackground {

			try {

				doInParallel(threadsCount = socket.threadsCount) {

					val buffer = ByteArray(length = socket.maxPacketSizeInBytes)
					val packet = DatagramPacket(buffer, buffer.length)

					loop {

						skippable {

							try {
								jvmSocket.receive(packet)
							} catch (_: InterruptedException) {
								stopThread
							} catch (_: InterruptedIOException) {
								stopThread
							} catch (_: IOException) {
								sleep(1.sec)
								skip
							} catch (e: Exception) {
								logW("Uncaught") { e }
								sleep(1.sec)
								skip
							}

							val socketAddress = SocketAddress(
								host = packet.address.hostAddress.orSkip,
								port = packet.port
							)

							val data = buffer.copyLimit(packet.length)

							try {
								socket.notifyReceived(socketAddress = socketAddress, data = data)
							} catch (e: Exception) {
								logW("Uncaught") { e }
							}

						}

						canBeStoppedHere

					}

				}

			} finally {
				assert { this.thread != null }
				this.thread = null
			}

		}

	}


	override fun send (socketAddress: SocketAddress, data: ByteArray) {
		try {
			jvmSocket?.send(
				DatagramPacket(
					data,
					data.length,
					InetAddress.getByName(socketAddress.host),
					socketAddress.port
				)
			)
		} catch (_: IOException) {
		} catch (e: Exception) {
			logW("Uncaught") { e }
		}
	}


	override fun closeNonBlocking() {

		this.thread!!.stopNonBlocking()

		this.jvmSocket!!.close()
		this.jvmSocket = null

		this.socket = null

	}


	override fun waitForClose() {
		sleepTill { this.thread == null }
	}


}