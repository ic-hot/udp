package ic.network.udp


import ic.base.primitives.int32.Int32
import ic.network.socket.SocketAddress
import ic.network.udp.impl.JvmUdpSocketImpl


abstract class UdpSocket {


	abstract val threadsCount : Int32

	open val maxPacketSizeInBytes : Int32 get() = 65507


	protected abstract fun onReceived (socketAddress: SocketAddress, data: ByteArray)

	internal fun notifyReceived (socketAddress: SocketAddress, data: ByteArray) {
		onReceived(socketAddress, data)
	}


	private val impl = JvmUdpSocketImpl()


	fun open (port: Int32) {
		impl.open(this, port = port)
	}


	fun send (address: SocketAddress, data: ByteArray) {
		impl.send(socketAddress = address, data = data)
	}


	fun closeNonBlocking() {
		impl.closeNonBlocking()
	}


	fun waitForClose() {
		impl.waitForClose()
	}


}