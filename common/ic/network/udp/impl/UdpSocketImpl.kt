package ic.network.udp.impl


import ic.base.primitives.int32.Int32
import ic.base.throwables.IoException

import ic.network.socket.SocketAddress
import ic.network.udp.UdpSocket


interface UdpSocketImpl {


	@Throws(IoException::class)
	fun open (socket: UdpSocket, port: Int32)


	fun send (socketAddress: SocketAddress, data: ByteArray)


	fun closeNonBlocking()

	fun waitForClose()


}