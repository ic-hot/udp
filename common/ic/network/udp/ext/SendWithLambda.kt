package ic.network.udp.ext


import ic.base.arrays.bytes.ByteArray
import ic.network.socket.SocketAddress
import ic.stream.output.ByteOutput

import ic.network.udp.UdpSocket


inline fun UdpSocket.send (address: SocketAddress, write: ByteOutput.() -> Unit) {

	send(
		address = address,
		data = ByteArray(write)
	)

}